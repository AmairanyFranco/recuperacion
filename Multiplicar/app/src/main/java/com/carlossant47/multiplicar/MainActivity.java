package com.carlossant47.multiplicar;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {

    private Spinner spMutiplicar;
    private Button btnEntrar;
    private Button btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayAdapter<String> tablas = new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_expandable_list_item_1,
                getResources().getStringArray(R.array.tablas));


        spMutiplicar = (Spinner) findViewById(R.id.spMutiplicar);
        btnEntrar = (Button) findViewById(R.id.btnEntrar);
        btnSalir = (Button) findViewById(R.id.btnSalir);

        spMutiplicar.setAdapter(tablas);
        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(MainActivity.this, MultiplicarActivity.class);
                intent.putExtra("tabla", spMutiplicar.getSelectedItemPosition() + 1);
                startActivity(intent);
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}

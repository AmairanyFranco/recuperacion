package com.carlossant47.multiplicar;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class MultiplicarActivity extends AppCompatActivity {

    private int num;
    private ListView lvTablas;
    private Button btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multiplicar);
        num = getIntent().getIntExtra("tabla", 1);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_expandable_list_item_1, getTabla(num));


        lvTablas = (ListView) findViewById(R.id.lvTablas);
        btnSalir = (Button) findViewById(R.id.btnSalir);

        lvTablas.setAdapter(adapter);
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    private ArrayList<String> getTabla(int num)
    {
        ArrayList<String> list = new ArrayList<>();

        for(int x = 1; x <= 10; x++)
        {
            int resultado = num * x;
            list.add(String.format("%s x %s = %s", String.valueOf(x),
                    String.valueOf(num), String.valueOf(resultado)));
        }
        return list;
    }
}
